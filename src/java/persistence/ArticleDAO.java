/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Article;

/**
 *
 * @author gonzalobenedicuevas
 */
public class ArticleDAO {
    private final Object lockOfTheConexion = new Object();
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://10.2.25.47/mvc"; //conexión desde Windows a la bbdd de Ubuntu
    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());
    private ResultSet rs;
    
    
//    Conectar
    public void connect() {
        try {
            Class.forName(ArticleDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(ArticleDAO.URL, "usuario", "usuario");
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }

//    DEsconectar
    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    
//    Volcar Todos
    public ArrayList<Article> getAll(){
        ArrayList<Article> articles = new ArrayList<>();
        try {
            stmt = connection.prepareStatement("select * from articulo");
            rs = stmt.executeQuery();
            while (rs.next()) {
                Article art = new Article(rs.getInt("id"), rs.getString("nombre"), rs.getDouble("precio"));
                articles.add(art);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return articles;
    }
    
//    Volcar por ID
    public Article getArticle(int id){
        Article art = new Article();
        try {
            stmt = connection.prepareStatement("select * from articulo where id=?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                art.setId(rs.getInt("id"));
                art.setNombre(rs.getString("nombre"));
                art.setPrecio(rs.getDouble("precio"));
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return art;
    }
//    Insertar
    public int insert(Article art){
        try {
            stmt = connection.prepareStatement("insert into articulo (nombre, precio) values (?, ?)");
            stmt.setString(1, art.getNombre());
            stmt.setDouble(2, art.getPrecio());
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
//    Modificar
    public int update(Article art){
        try {
            stmt = connection.prepareStatement("update articulo set nombre=?, precio=? where id=?");
            stmt.setString(1, art.getNombre());
            stmt.setDouble(2, art.getPrecio());
            stmt.setInt(3, art.getId());
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
//    Eliminar
    public int delete(int id){
        try {
            stmt = connection.prepareStatement("delete from articulo where id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
}
