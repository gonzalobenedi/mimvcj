package persistence;

/*
 * Clase que se ocupa de la persistencia de USER
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Rafa
 */
public class UserDAO {
    private final Object lockOfTheConexion = new Object();
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
    //private final String URL="jdbc:mysql://londonapp.cufhmzckqhkn.eu-west-1.rds.amazonaws.com:3306/mvc";//conexión con bbdd AWS
    private static final String URL = "jdbc:mysql://10.2.25.47/mvc"; //conexión desde Windows a la bbdd de Ubuntu
    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());
    private ResultSet rs;

    //metodo connect
    public void connect() {
        try {
            Class.forName(UserDAO.CLASS_DRIVER);
            //connection = DriverManager.getConnection(UserDAO.URL, "gonzalonbc", "rickety450lapse"); //conexion AWS
            connection = DriverManager.getConnection(UserDAO.URL, "usuario", "usuario");//conexion localhost Ubuntu
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
//metodo disconnect

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    
//    Volcar Todos
    public ArrayList<User> getAll() {
        ArrayList<User> users = null;        
        try {
            stmt = connection.prepareStatement("select * from user");
            rs = stmt.executeQuery();
            users = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                User user = new User();
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setLogin(rs.getString("login"));
                //other properties
                users.add(user);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return users;
    }
    
//    Volcar por ID
    public User getUser(int id) throws SQLException{
        try{
            stmt = connection.prepareStatement("select * from user where id=?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            User user = new User();
            if(rs.next()){
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
            }
            return user;
        }catch(SQLException e){
            LOG.log(Level.SEVERE, null, e);
        }
        return null;
    }
    
//    Insertar
    public int insert(User user) {
        try {
            stmt = connection.prepareStatement("insert into user values(DEFAULT, ?, ?, ?, DEFAULT)");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getSurname());
            stmt.setString(3, user.getLogin());

            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
//    Update
    public int update(User user){
        try {
            stmt = connection.prepareStatement("update user set name=?, surname=?, login=?, password=? where id=?");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getSurname());
            stmt.setString(3, user.getLogin());
            stmt.setString(4, user.getPassword());
            stmt.setInt(5, user.getId());
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }

//    Delete
    public int delete(int id){
        try{
            stmt = connection.prepareStatement("delete from user where id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate();
        }catch(SQLException e){
            LOG.log(Level.SEVERE, null, e);
            return 0;
        }
        
    }
    
    
}
