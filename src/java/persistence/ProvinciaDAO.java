/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Provincia;

/**
 *
 * @author usuario
 */
public class ProvinciaDAO {
    private final Object lockOfTheConexion = new Object();
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
//    private static final String URL = "jdbc:mysql://10.2.25.47/mvc"; //conexión desde Windows a la bbdd de Ubuntu
    private static final String URL="jdbc:mysql://londonapp.cufhmzckqhkn.eu-west-1.rds.amazonaws.com:3306/mvc";//conexión con bbdd AWS
    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());
    private ResultSet rs;
    
    
//    Conectar
    public void connect() {
        try {
            Class.forName(ProvinciaDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(ProvinciaDAO.URL, "gonzalonbc", "rickety450lapse"); //conexion AWS
//            connection = DriverManager.getConnection(ProvinciaDAO.URL, "usuario", "usuario");
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
    
    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Provincia> getAll(int pagina, int tamanioPag){
        ArrayList<Provincia> provincias = new ArrayList<>();
        try {
            int limite = (pagina-1)*tamanioPag;
            stmt = connection.prepareStatement("select * from provincia limit ? offset ?");
            stmt.setInt(1, tamanioPag);
            stmt.setInt(2, limite);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Provincia p = new Provincia(rs.getInt("id"), rs.getString("provincia"));
                provincias.add(p);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return provincias;
    }
    
    public Provincia getProvincia(int id){
        Provincia p = null;
        try {
            stmt = connection.prepareStatement("select * from provincia where id=?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                p = new Provincia(rs.getInt("id"), rs.getString("provincia"));
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return p;
    }
    
    public int delete(int id){
        try {
            stmt = connection.prepareStatement("delete from provincia where id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
