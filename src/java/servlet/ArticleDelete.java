/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistence.ArticleDAO;

/**
 *
 * @author gonzalobenedicuevas
 */
public class ArticleDelete extends HttpServlet {
    ArticleDAO articleDao = new ArticleDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        articleDao.connect();
        
        //Borro de la bbdd el usuario con el id que llega como parametro.
        
        int resultado = articleDao.delete(Integer.valueOf(request.getParameter("id")));

        //Forward a resultado.jsp o error.jsp dependiendo del resultado de la consulta.
        
        String path = "";
        RequestDispatcher dispatcher = null;
        if (resultado==0) {
            path = "/WEB-INF/view/article/error.jsp";
            request.setAttribute("message", "Lo sentimos, ha habido un problema a la hora de eliinar el articulo.");
            //setear dispatcher y forward.
        }else{
            path = "/WEB-INF/view/article/resultado.jsp";
            request.setAttribute("message", "Se ha eliminado el articulo correctamente.");
            //setear dispatcher y forward.
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
