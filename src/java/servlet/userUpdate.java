/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import persistence.UserDAO;

/**
 *
 * @author usuario
 */
@WebServlet(name = "userUpdate", urlPatterns = {"/userUpdate"})
public class userUpdate extends HttpServlet {
    UserDAO userDao = new UserDAO();
    private static final Logger LOG = Logger.getLogger(userNew.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        userDao.connect();
        
        //Recojo los datos del usuario a modificar que me llegan de update.jsp.
        
        User user = new User();
        user.setId(Integer.valueOf(request.getParameter("id")));
        user.setName(request.getParameter("name"));
        user.setSurname(request.getParameter("surname"));
        user.setLogin(request.getParameter("login"));
        user.setPassword(request.getParameter("password"));
        
        //Llamo al metodo update de UserDAO.
        
        int resultado = userDao.update(user);
        
        //Forward a resultado.jsp o error.jsp dependiendo del resultado de la consulta.
        
        String path = "";
        RequestDispatcher dispatcher = null;
        if (resultado==0) {
            path = "/WEB-INF/view/user/error.jsp";
            request.setAttribute("message", "Lo sentimos, ha habido un problema a la hora de actualizar al usuario " + user.getLogin());
            //setear dispatcher y forward.
        }else{
            path = "/WEB-INF/view/user/resultado.jsp";
            request.setAttribute("message", "El usuario "+user.getLogin()+" ha sido actualizado correctamente.");
            //setear dispatcher y forward.
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
