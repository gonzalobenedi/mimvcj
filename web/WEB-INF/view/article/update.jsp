<%-- 
    Document   : update
<%-- 
    Document   : update

    Created on : 30-nov-2016, 21:17:50
    Author     : usuario
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/articulo">Articulo</a>
    <a href="<%= request.getContextPath() %>/user">Usuarios</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        <h1>Modificar ArtÃ­culo</h1>
        <jsp:useBean id="articulo" class="model.Article" scope="request"/>
        <form action="" method="post">
            <p><label>Nombre: <input type="text" name="nombre" value="<%articulo.getNombre();%>"></label></p>
            <p><label>Precio: <input type="text" name="precio" value="<%articulo.getPrecio();%>"></label></p>
            <input type="hidden" name="id" value="<%articulo.getId();%>">
            <input type="submit" value="Actualizar">
        </form>

    </div>
    
    <div id="footer"> Pie de pÃ¡gina</div>
</body>
</html>