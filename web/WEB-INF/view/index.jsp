<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/articulo">Articulo</a>
    <a href="<%= request.getContextPath() %>/province/index/1">Provincias</a>
    <a href="<%= request.getContextPath() %>/user/index">Usuarios</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        
        <h1>Bienvenido al Framework de JavaEE de 2016</h1>

    </div>
    
    <div id="footer"> Pie de p�gina</div>

</body>
</html>
