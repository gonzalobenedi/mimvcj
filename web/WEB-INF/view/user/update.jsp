<%-- 
    Document   : update
    Created on : 23-nov-2016, 20:05:05
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/articulo">Articulo</a>
    <a href="<%= request.getContextPath() %>/user">Usuarios</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        <h1>Modificar Usuario</h1>
        <jsp:useBean id="user" class="model.User" scope="request"/>
        <form action="" method="post">
            <p><label>Nombre: <input type="text" value="<%user.getName();%>"></label></p>
            <p><label>Apellidos: <input type="text" value="<%user.getSurname();%>"></label></p>
            <p><label>Login: <input type="text" value="<%user.getLogin();%>"></label></p>
            <p><label>Contraseña: <input type="password" value="<%user.getPassword();%>"></label></p>
            <input type="submit">
        </form>

    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
