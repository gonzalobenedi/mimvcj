<%-- 
    Document   : details
    Created on : 07-dic-2016, 20:02:32
    Author     : usuario
--%>

<%@page import="model.Provincia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/articulo">Articulo</a>
    <a href="<%= request.getContextPath() %>/province/index/1">Provincias</a>
    <a href="<%= request.getContextPath() %>/user/index">Usuarios</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        
        <h1>Detalles de provincia</h1>
        <jsp:useBean id="provincia" class="Provincia" scope="request"/>
        <p>ID: <%=provincia.getId()%> || Nombre: <%=provincia.getProvincia()%></p>
</div>
    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
