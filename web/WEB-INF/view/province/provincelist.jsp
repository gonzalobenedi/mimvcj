<%-- 
    Document   : provincelist
    Created on : 07-dic-2016, 20:01:37
    Author     : usuario
--%>

<%@page import="model.Provincia"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/articulo">Articulo</a>
    <a href="<%= request.getContextPath() %>/province/index/1">Provincias</a>
    <a href="<%= request.getContextPath() %>/user/index">Usuarios</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>

</div>

    
    <div id="content">
        
        <h1>Listado de Provincias</h1>
        
        <jsp:useBean id="provincias" class="ArrayList<model.Provincia>" scope="request"/>  
        <p><a href="<%= request.getContextPath() %>/province/seek">Buscar provincia</a></p>
    <table>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Recordar</th>
            <th>Eliminar</th>
        </tr>
    <%
        Iterator<model.Provincia> iterator = provincias.iterator();
        while (iterator.hasNext()) {
            Provincia p = iterator.next();%>
    <tr>
        <td><%= p.getId()%></td>
        <td><%= p.getProvincia()%></td>
        <td><a href="<%= request.getContextPath()%>/province/remember/<%= p.getProvincia()%>">Recordar</a></td>
        <td><a href="<%= request.getContextPath()%>/province/delete/<%= p.getId()%>">Borrar</a></td>
    </tr>
    <%
        }
    %>
    </table>
    <p>Página:
    <%
        for (int i=0; i<6;i++){
            %>
            <a href="<%= request.getContextPath() %>/province/index/<%= i+1%>"><%= i+1 %></a>
        <%
        }
        %>
    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
